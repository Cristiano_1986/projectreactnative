import React from "react";
import { View, Text } from "react-native";
import { styles } from "./styles";
import { Avatar } from "../Avatar";

export function Profile() {
    return (
        <View style={styles.container}>
            <Avatar urlImage="https://www.imagenspng.com.br/wp-content/uploads/2015/02/super-mario-mario-14.png"/>
            <View style={styles.user}>
                <Text style={styles.greeting}>
                    Ola,
                </Text>
                <Text style={styles.username}>
                    Cristiano
                </Text>
            </View>
            <Text style={styles.message}>
                Hoje é dia de vitória
            </Text>
        </View>
    )
}