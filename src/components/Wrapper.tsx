import React from "react";
import { View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

// import { Container } from './styles';

const Wrapper: React.FC = ({ children }) => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: "#0c1238",
      }}
    >
      {children}
    </SafeAreaView>
  );
};

export { Wrapper };
